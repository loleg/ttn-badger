# -*- coding: utf-8 -*-
"""User models."""
import datetime as dt
from json import loads

from ttn_badger.database import Column, PkModel, db, reference_col, relationship
from ttn_badger.extensions import bcrypt

from .utils import timesince

class Badge(PkModel):
    """A badge definition."""

    __tablename__ = "badges"
    region = Column(db.String(10), nullable=False)
    appid = Column(db.String(32), nullable=False)
    devid = Column(db.String(32), nullable=False)
    appkey = Column(db.String(128), nullable=False)
    created_at = Column(db.DateTime, nullable=False, default=dt.datetime.utcnow)
    updated_at = Column(db.DateTime, nullable=False, default=dt.datetime.utcnow)
    response = Column(db.UnicodeText, nullable=True)

    @property
    def last_seen(self):
        """Get last device seen."""
        json = loads(self.response)
        seen = int(json['lorawan_device']['last_seen'])
        seen = dt.fromtimestamp(seen)
        seen = timesince(seen)
        return f"{seen}"

    def __repr__(self):
        """Represent instance as a unique string."""
        return f"<Badge({self.devid!r})>"
